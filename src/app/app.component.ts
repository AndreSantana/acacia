import { Component } from '@angular/core';
import { EmmiterService } from './services/emmiters/emmiter';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  private currentUser: any
  public isCollapsed: boolean = true;
  constructor(private router: Router, private emmiter: EmmiterService) {
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser')) || null;
    }
    emmiter.eventEmmited$.subscribe(user => this.currentUser = user);
  }

  isAdmin() {
    if (this.currentUser != undefined) {
      if (this.currentUser.roles[0].name === 'admin') {
        return true;
      }
      else {
        return false;
      }
    } else {
      return false;
    }
  }

  logout() {
    this.currentUser = null;
    this.router.navigate(['login']);
    localStorage.removeItem('currentUser');
  }


  ngOnInit() {
  }

}
