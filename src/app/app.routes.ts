import { ModuleWithProviders }  from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { TeacherComponent } from './teacher/teacher.component';
import { ConfigurationsComponent } from './components/admin_components/configurations/configurations.component';
import { CreateUserComponent } from './components/admin_components/create-user/create-user.component';
import { CreateSessionComponent } from './components/admin_components/create-session/create-session.component';
import { AdvancedOptionsComponent } from './components/teacherComponents/advanced-options/advanced-options.component';
import { CreateObservationComponent } from './components/commonComponents/create-observation/create-observation.component';
import { SearchUserComponent } from './components/commonComponents/search-user/search-user.component';
import { SearchSessionComponent } from './components/commonComponents/search-session/search-session.component';
import { SearchObservationComponent } from './components/commonComponents/search-observation/search-observation.component';
import { SearchMusicObservationComponent } from './components/commonComponents/search-music-observation/search-music-observation.component';
import { StudentMusicSelectionComponent } from './student-music-selection/student-music-selection.component';
import { CourseVisualizationComponent } from './components/studenComponents/course-visualization/course-visualization.component';
import { AttentionAutoevaluationComponent } from './components/studenComponents/attention-autoevaluation/attention-autoevaluation.component';
import { AnnalistComponent } from './annalist/annalist.component';
import { FeaturesSearchVisualizationComponent } from './components/annalistComponents/features-search-visualization/features-search-visualization.component';
import { ClassifiersSearchVisualizationComponent } from './components/annalistComponents/classifiers-search-visualization/classifiers-search-visualization.component';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'admin/configurations', component: ConfigurationsComponent },
  { path: 'admin/createUser', component: CreateUserComponent },
  { path: 'admin/createSession', component: CreateSessionComponent },
  { path: 'admin/createObservation', component: CreateObservationComponent },
  { path: 'admin/searchUser', component: SearchUserComponent },
  { path: 'admin/searchSession', component: SearchSessionComponent },
  { path: 'admin/searchObservation', component: SearchObservationComponent },
  { path: 'admin/searchMusicObservation', component: SearchMusicObservationComponent },
  { path: 'teacher', component: TeacherComponent },
  { path: 'teacher/advancedOptions', component: AdvancedOptionsComponent },
  { path: 'teacher/advancedOptions/createObservation', component: CreateObservationComponent },
  { path: 'teacher/advancedOptions/searchUser', component: SearchUserComponent },
  { path: 'teacher/advancedOptions/searchSession', component: SearchSessionComponent },
  { path: 'teacher/advancedOptions/searchObservation', component: SearchObservationComponent },
  { path: 'teacher/advancedOptions/searchMusicObservation', component: SearchMusicObservationComponent },
  { path: 'studentMusicSelection', component: StudentMusicSelectionComponent },
  { path: 'studentMusicSelection/courseVisualization', component: CourseVisualizationComponent },
  { path: 'studentMusicSelection/attentionAutoEvaluation', component: AttentionAutoevaluationComponent },
  { path: 'annalist', component: AnnalistComponent },
  { path: 'annalist/featuresSearchVisualization', component: FeaturesSearchVisualizationComponent },
  { path: 'annalist/classifiersSearchVisualization', component: ClassifiersSearchVisualizationComponent },
  { path: '', component: HomeComponent},  
  { path: '**', component: HomeComponent }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
