// Imports
import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { EmmiterService } from './services/emmiters/emmiter';

//Routes
import { Routing } from './app.routes';
import { AppComponent } from './app.component';
import { AlertModule } from 'ngx-bootstrap';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { TeacherComponent } from './teacher/teacher.component';
import { ConfigurationsComponent } from './components/admin_components/configurations/configurations.component';
import { CreateUserComponent } from './components/admin_components/create-user/create-user.component';
import { CreateSessionComponent } from './components/admin_components/create-session/create-session.component';
import { AdvancedOptionsComponent } from './components/teacherComponents/advanced-options/advanced-options.component';
import { CreateObservationComponent } from './components/commonComponents/create-observation/create-observation.component';
import { SearchUserComponent } from './components/commonComponents/search-user/search-user.component';
import { SearchSessionComponent } from './components/commonComponents/search-session/search-session.component';
import { SearchObservationComponent } from './components/commonComponents/search-observation/search-observation.component';
import { SearchMusicObservationComponent } from './components/commonComponents/search-music-observation/search-music-observation.component';
import { StudentMusicSelectionComponent } from './student-music-selection/student-music-selection.component';
import { CourseVisualizationComponent } from './components/studenComponents/course-visualization/course-visualization.component';
import { AttentionAutoevaluationComponent } from './components/studenComponents/attention-autoevaluation/attention-autoevaluation.component';
import { AnnalistComponent } from './annalist/annalist.component';
import { FeaturesSearchVisualizationComponent } from './components/annalistComponents/features-search-visualization/features-search-visualization.component';
import { ClassifiersSearchVisualizationComponent } from './components/annalistComponents/classifiers-search-visualization/classifiers-search-visualization.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    ConfigurationsComponent,
    CreateUserComponent,
    CreateSessionComponent,
    TeacherComponent,
    AdvancedOptionsComponent,
    CreateObservationComponent,
    SearchUserComponent,
    SearchSessionComponent,
    SearchObservationComponent,
    SearchMusicObservationComponent,
    StudentMusicSelectionComponent,
    CourseVisualizationComponent,
    AttentionAutoevaluationComponent,
    AnnalistComponent,
    FeaturesSearchVisualizationComponent,
    ClassifiersSearchVisualizationComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    AlertModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    Routing
  ],
  providers: [
    EmmiterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
