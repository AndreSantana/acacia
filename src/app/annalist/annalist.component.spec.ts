import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnalistComponent } from './annalist.component';

describe('AnnalistComponent', () => {
  let component: AnnalistComponent;
  let fixture: ComponentFixture<AnnalistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnalistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnalistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
