import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styles: []
})
export class CreateUserComponent  {
  private response: any;
    constructor (
      private userService : UsersService,
      private router: Router
    ){

    }
    submitForm(form: any): void{
      this.userService.createUser(
        form.kindUser,
        form.gender,
        form.name,
        form.age,
        form.education_Degree,
        form.area_of_Degree
      ).subscribe(
        response => this.response = response,
        error => console.log(error),
        () => this.handleSuccess()
      );
    }

    handleSuccess(){
      this.router.navigate(['login',{ message: 'Se ha registrado satisfactoriamente' }]);
    }
}
