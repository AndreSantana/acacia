import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchObservationComponent } from './search-observation.component';

describe('SearchObservationComponent', () => {
  let component: SearchObservationComponent;
  let fixture: ComponentFixture<SearchObservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchObservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
