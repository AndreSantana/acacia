import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchMusicObservationComponent } from './search-music-observation.component';

describe('SearchMusicObservationComponent', () => {
  let component: SearchMusicObservationComponent;
  let fixture: ComponentFixture<SearchMusicObservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchMusicObservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMusicObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
