import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttentionAutoevaluationComponent } from './attention-autoevaluation.component';

describe('AttentionAutoevaluationComponent', () => {
  let component: AttentionAutoevaluationComponent;
  let fixture: ComponentFixture<AttentionAutoevaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttentionAutoevaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttentionAutoevaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
