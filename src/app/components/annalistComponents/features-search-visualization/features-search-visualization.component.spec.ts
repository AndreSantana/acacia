import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesSearchVisualizationComponent } from './features-search-visualization.component';

describe('FeaturesSearchVisualizationComponent', () => {
  let component: FeaturesSearchVisualizationComponent;
  let fixture: ComponentFixture<FeaturesSearchVisualizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturesSearchVisualizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesSearchVisualizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
