import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifiersSearchVisualizationComponent } from './classifiers-search-visualization.component';

describe('ClassifiersSearchVisualizationComponent', () => {
  let component: ClassifiersSearchVisualizationComponent;
  let fixture: ComponentFixture<ClassifiersSearchVisualizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassifiersSearchVisualizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifiersSearchVisualizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
