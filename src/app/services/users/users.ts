import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiEndpoint } from '../api';
import 'rxjs/add/operator/map';

@Injectable()
export class UsersService {
  constructor(private http: Http) { }

  createUser(kindUser: string, gender: string, name: string, age: number, education_Degree: string, area_of_Degree: string){
    const payload = {
      Gender: gender,
      Name: name,
      Age: age,
      Education_Degree: education_Degree,
      Area_of_Degree: area_of_Degree
    }
    return this.http.post(`${ApiEndpoint}/insert/${kindUser}`,payload)
      .map(response => response.json());
  }

  updateUser(id:number, name: string, age: string, education_Degree: string, area_of_Degree: string){
   const payload = {
     Name: name,
     Age: age,
     Education_Degree: education_Degree,
     Area_of_Degree: area_of_Degree
   }
   return this.http.put(`${ApiEndpoint}/users/${id}`,payload)
     .map(response => response.json());
  }

}
