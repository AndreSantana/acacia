import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentMusicSelectionComponent } from './student-music-selection.component';

describe('StudentMusicSelectionComponent', () => {
  let component: StudentMusicSelectionComponent;
  let fixture: ComponentFixture<StudentMusicSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentMusicSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentMusicSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
